const wallColor = "black";
const controlledColor = "red";
const pieceColor = "darkred";
const standardColor = "white";
const unsafeColor = "violet";
const table = document.getElementById("board");
const playerColors = {
	0: "darkRed",
	1: "darkBlue",
	2: "darkGreen",
}

class PlayMode {
	static getNumPlayers() { return 1; }
	static getHigherIsBetter() { return true; }
}

class Standard extends PlayMode {}
class TwoPlayerStandard extends PlayMode {
	static getNumPlayers() { return 2; }
}
class Minimize extends PlayMode {
	static getHigherIsBetter() { return false; }
}

function tableAt(row, col) {
	return table.children[row].children[col];
}

let smiley = document.getElementById("smiley");
function displayWin() {
	smiley.innerHTML = "☺";
	smiley.style.color = "green";
}
function displayLoss() {
	smiley.innerHTML = "☹";
	smiley.style.color = "red";
}
function displayIncompleteGame() {
	smiley.innerHTML = "😐";
	smiley.style.color = "yellow";
}

function setMessageBox(message) {
	document.getElementById("message-box").innerHTML = message;
}

class Level {
	constructor(board, pieceType, target, pieceDict={}, playMode=Standard, message="") {
		this.board = board;
		this.pieceType = pieceType;
		this.target = target;
		//this.higherIsBetter = PlayMode.getHigherIsBetter();
		this.solved = false;
		this.piecesOnBoard = [];
		this.unsafeSquares = [];
		this.pieceDict = pieceDict;
		this.playMode = playMode;
		this.activePlayer = 0;
		this.blankSquares = [];
		this.message = message;
	}
	start() {
		this.initializeBoard(this.board);
		this.updateCounter();
		this.colorBoard();
		setMessageBox(this.message);
		displayIncompleteGame();
	}
	getControlled() {
		let controlled = new Set();
		for (var piece of this.piecesOnBoard) {
			for (var space of piece.moveSet) {
				controlled.add(space);
			}
		}
		return controlled;
	}
	tryPlacePiece(row, col) {
		if (this.getControlled().has(JSON.stringify({row: row, col: col}))
			|| this.board[row][col] == 0
		) { return false; }
		let piece = new this.pieceType(row, col, this.board, this.activePlayer);
		for (var existingPiece of this.piecesOnBoard) {
			if (piece.moveSet.has(JSON.stringify(
					{row: existingPiece.row, col: existingPiece.col}
				))
			) { return false; }
		}
		for (var square of this.unsafeSquares) {
			let squareObj = JSON.parse(square);
			for (var possibleMove of piece.moveSet) {
				let moveObj = JSON.parse(possibleMove);
				if (squareObj.row == moveObj.row 
				    && squareObj.col == moveObj.col
				) {
					return false;
				}
			}
		}
		this.piecesOnBoard.push(piece);
		this.colorBoard();
		this.updateCounter();
		let nonFreeSquares = this.getControlled().size
		                     + this.unsafeSquares.length 
		                     + this.blankSquares.length;
		let allSquares = this.board.length * this.board[0].length
		if (this.playMode.getNumPlayers() == 1) {
			if (this.playMode.getHigherIsBetter()) {
				if (this.piecesOnBoard.length >= this.target) {
					displayWin();
				} else if (nonFreeSquares >= allSquares) {
					displayLoss();
				}
			} else {	
				if (this.piecesOnBoard.length > this.target) {
					displayLoss()
				} else if (nonFreeSquares >= allSquares) {
					displayWin();
				}
			}
		}
		this.activePlayer = (this.activePlayer + 1) 
		                    % this.playMode.getNumPlayers();
		return true;
	}
	colorBoard() {
		for (var unsafeSquare of this.unsafeSquares) {
			let square = JSON.parse(unsafeSquare);
			tableAt(square.row, square.col).style.backgroundColor = unsafeColor;
		}
		for (var spaceString of this.getControlled()) {
			let space = JSON.parse(spaceString);
			tableAt(space.row, space.col).style.backgroundColor = controlledColor;
		}
		for (var piece of this.piecesOnBoard) {
			tableAt(piece.row, piece.col).style.backgroundColor 
				= playerColors[piece.placedBy];
		}
	}
	initializeBoard(boardArray) {
		let board = boardArray;
		this.piecesOnBoard = [];
		this.blankSquares = [];
		for (let i = 0; i < board.length; i++) {
			for (let j = 0; j < board[0].length; j++) {
				if (this.board[i][j] == 0) {
					this.blankSquares.push(JSON.stringify({row: i, col: j}));	
				} else if (this.board[i][j] == 1) {
					
				} else if (this.board[i][j] == 2) {
					this.unsafeSquares.push(JSON.stringify({row: i, col: j}));	
				} else if (this.board[i][j] in this.pieceDict) {
					//this.piecesOnBoard.push(new Pawn(i, j, this.board));
					this.piecesOnBoard.push(new this.pieceDict[this.board[i][j]](
						i, j, this.board, Number.MAX_SAFE_INTEGER
					));
				} else {
					throw "Unknown item in board array: " + this.board[i][j];
				}
			}
		}
		
		table.innerHTML = "";
		for (let i = 0; i < board.length; i++) {
			let row = board[i];
			let tableRow = document.createElement("tr");
			for (let j = 0; j < row.length; j++) {
				let col = row[j];
				let tableCol = document.createElement("td");
				tableCol.addEventListener("click", () => {
					this.tryPlacePiece(i, j)
				}, false);
				if (col == 0) {
					tableCol.style.backgroundColor = wallColor;
				}
				tableRow.appendChild(tableCol);
			}
			table.appendChild(tableRow);
		}
		
		
	}
	updateCounter() {
		document.getElementById("counter").innerHTML 
			= this.piecesOnBoard.length + "/" + this.target;
	}
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomBoard() {
	let numRows = getRandomInt(4, 12);
	let numCols = getRandomInt(4, 12);
	let board = []
	for (let i = 0; i < numRows; i++) {
		row = [];
		
		for (let j = 0; j < numCols; j++) {
			if (Math.random() > .85) {
				row.push(0);
			} else {
				row.push(1);
			}
		}
		board.push(row);
	}
	return board;
}

let levels = [
	new Level(randomBoard(), Lancer, 5, {}, Standard, "This level is randomly generated. You probably shouldn't be seeing this if you're an end-user."),
	new Level([[1,1,1,0,1,1,1,1,1,1,0],[1,1,1,0,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1,1,1,1],[0,1,1,1,1,1,1,1,1,1,1],[1,1,1,1,0,0,0,1,1,1,1],[1,1,0,1,1,1,0,1,1,0,1],[0,1,1,1,1,1,1,1,1,1,1],[1,1,0,1,1,1,1,1,1,0,1]], Lancer, 31, {}, Standard, "Don't forget to move this level to a better place."),
	/*new Level([], Bishop, 0, {}, Standard, "Welcome to the game!"),*/
	new Level([[1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],], Bishop, 9, {}, Standard, 
			   "<p>Welcome to Nqueens! To get started, try putting some pieces on the board.</p>"),
	new Level([[[1],[1],[1],[1],[1],[1]],[[1],[1],[1],[1],[1],[1]],[[1],[1],[1],[1],[1],[1]],[[1],[1],[1],[0],[1],[1]]], Bishop, 8, {}, Standard,
	          "<p>As you can see, when you put down a piece, it exhausts the tiles around it in a specific pattern</p>"
			  +"<p>For now, the pieces you use will exhaust all the tiles that line up with them <b>diagonally</b>. Later on, you'll learn to use a number of different pieces.</p><p>Your goal is to fit <b>as many pieces as possible</b> on the board at once. To advance to the next level, you must fit at least the number specified in the denominator below the board."),
	new Level([[0, 1, 1, 0, 0, 1, 1, 0,],
	           [1, 1, 1, 1, 1, 1, 1, 1,],
			   [1, 1, 0, 0, 1, 1, 1, 1,],
			   [1, 1, 1, 1, 1, 1, 1, 1,],
			   [0, 1, 1, 0, 0, 1, 1, 0,]], Bishop, 11, {},),
	new Level([[0, 0, 1, 1, 1],
	           [1, 0, 1, 0, 1],
			   [1, 1, 1, 0, 1],
			   [1, 0, 0, 1, 0]], Bishop, 8, {},),
	new Level([[[1],[1],[1],[0],[1],[1],[1],[1],[1],[1],[1],[1]],
	           [[0],[1],[1],[1],[0],[0],[0],[1],[1],[1],[1],[1]],
			   [[1],[1],[1],[1],[1],[1],[0],[1],[1],[1],[1],[1]],
			   [[1],[1],[1],[1],[1],[1],[0],[1],[1],[1],[0],[1]],
			   [[1],[0],[0],[1],[1],[0],[1],[1],[1],[1],[1],[1]]],
			   Bishop, 18),
	new Level([[1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],], Knight, 5, {}, Standard, 
			   "<p>Let's move on to a new piece.</p>"),
	
	/*new Level([[1, 1, 1, 1],
	           [1, 1, 1, 1],
			   [1, 1, 1, 1],
			   [1, 1, 1, 1]], Bishop, 6, {3: Pawn, 4: Queen}),*/
	
	
	new Level([[0, 0, 1, 1, 1, 1, 1, 0],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 0, 0, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1],
			   [0, 1, 1, 1, 1, 1, 1, 0]], Queen, 9),
	new Level([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
			   [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	           [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]], Queen, 11, {}, TwoPlayerStandard),
	new Level([[1, 1, 0, 1, 0, 1, 1, 1],
	           [1, 1, 1, 0, 0, 0, 1, 1],
			   [1, 0, 1, 1, 1, 1, 0, 1],
			   [1, 1, 0, 1, 0, 1, 1, 0],
			   [1, 1, 1, 0, 0, 0, 1, 1]], Knight, 16)
]
levelNumber = -1;

function jumpLevel(n) {
	let newLevelNumber = levelNumber + n;
	if (newLevelNumber < 0 || newLevelNumber >= levels.length) {
		return;
	}
	levelNumber = newLevelNumber;
	levels[levelNumber].start();
}

jumpLevel(1);

	
