class Piece {
	constructor(row, col, board, placedBy) {
		this.row = row;
		this.col = col;
		this.board = board;
		this.moveSet = this.getMoves();
		this.placedBy = placedBy;
	}
	getMoves() {
		let moveSet = new Set(JSON.stringify({row: this.row, col: this.col}));
	}
}

class King extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (let i = -1; i < 2; i++) {
			for(let j = -1; j < 2; j++) {
				let checkPos = {row: this.row + i, col: this.col + j};
				if (!(isInvalid(checkPos, this.board))) {
					moveSet.add(JSON.stringify(checkPos));
				}
			}
		}
		return moveSet;	
	}
}

class Pawn extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (var move of [{row: this.row - 1, col: this.col + 1},
		                  {row: this.row - 1, col: this.col - 1},
						  {row: this.row, col: this.col}]) {
			if (!isInvalid(move, this.board)) {
				moveSet.add(JSON.stringify(move));
			}
		}
		return moveSet;
	}
}

class Knight extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (var x of [1, -1]) {
			for (var y of [1, -1]) {
				for (var space of [
					{ row: this.row + (y * 2) * x, col: this.col + x },
					{ row: this.row + x, col: this.col + (y * 2) * x }
				]) {
					if (!(isInvalid(space, this.board))) { 
						moveSet.add(JSON.stringify(space));
					}
				}
			}
		}
		return moveSet;
	}
}

class Lancer extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (var sign of [1, -1]) {
			let dir = true;
			for (var space of [{row: this.row, col: this.col},
							   {row: this.row - sign, col: this.col},
							   {row: this.row - (2 * sign), col: this.col}]) {
				if (isInvalid(space, this.board)) {
					dir = false;
					break;
				} else { moveSet.add(JSON.stringify(space)); }
			}
			if (dir) {
				for (var space of [{row: this.row - (2 * sign), col: this.col + 1},
								   {row: this.row - (2 * sign), col: this.col - 1}]) {
					if (!isInvalid(space, this.board)) {
						moveSet.add(JSON.stringify(space));
					}
				}
			}
			/*dir = true;
			for (var space of [{col: this.col, row: this.row},
							   {col: this.col - sign, row: this.row},
							   {col: this.col - (2 * sign), row: this.row}]) {
				if (isInvalid(space, this.board)) {
					dir = false;
					break;
				} else { moveSet.add(JSON.stringify(space)); }
			}
			if (dir) {
				for (var space of [{col: this.col - (2 * sign), row: this.row + 1},
								   {col: this.col - (2 * sign), row: this.row - 1}]) {
					if (!isInvalid(space, this.board)) {
						moveSet.add(JSON.stringify(space));
					}
				}
			}*/
		}
		return moveSet;
	}
}

class Bishop extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() { return diagonalLines(this.row, this.col, this.board); }
}

class Rook extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() { return cardinalLines(this.row, this.col, this.board); }
}

class Queen extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() { return union(diagonalLines(this.row, this.col, this.board),
	                          cardinalLines(this.row, this.col, this.board)); }
}

function isInvalid(obj, theBoard) {
	//console.log(obj);
	//console.log(theBoard);
	if (obj.row < 0 || obj.row >= theBoard.length || obj.col < 0 
	    || obj.col >= theBoard[obj.row].length || theBoard[obj.row][obj.col] == 0
	) { return true; }
	return false;
}

class Goblin extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (var move of [{row: this.row - 1, col: this.col},
		                  {row: this.row - 2, col: this.col + 2},
		                  {row: this.row - 2, col: this.col + 1},
						  {row: this.row + 1, col: this.col},
						  {row: this.row + 2, col: this.col - 2},
						  {row: this.row + 2, col: this.col - 1},
						  {row: this.row, col: this.col}]) {
			if (!isInvalid(move, this.board)) {
				moveSet.add(JSON.stringify(move));
			}
		}
		return moveSet;
	}
}

class Birdie extends Piece {
	constructor(row, col, board, placedBy) { super(row, col, board, placedBy); }
	getMoves() {
		let moveSet = new Set();
		for (let i = -1; i < 2; i++) {
			for(let j = -1; j < 2; j++) {
				let checkPos = {row: this.row + i, col: this.col + j};
				if (!(isInvalid(checkPos, this.board))) {
					moveSet.add(JSON.stringify(checkPos));
				}
			}
		}
		for (var move of [{row: this.row, col: this.col},

		                  {row: this.row + 2, col: this.col + 2},
						  {row: this.row + 2, col: this.col - 2},
						  {row: this.row - 2, col: this.col + 2},
						  {row: this.row - 2, col: this.col - 2},]) {
			if (!isInvalid(move, this.board)) {
				moveSet.add(JSON.stringify(move));
			}
		}
		return moveSet;
	}
}



function lines(row, col, board, listOfAugmentPairs) {
	moveSet = new Set();
	for (dir of listOfAugmentPairs) {
		let checkPos = {row: row, col: col};
		while (!isInvalid(checkPos, board)) {
			moveSet.add(JSON.stringify({row: checkPos.row, col: checkPos.col}));
			checkPos.row += dir.row; checkPos.col += dir.col;
		}
	}
	return moveSet;

}

function diagonalLines(row, col, board) {
	return lines(row, col, board, [{row: 1, col: 1}, {row: 1, col: -1}, 
	                               {row: -1, col: 1}, {row: -1, col: -1}]);
}

function cardinalLines(row, col, board) {
	return lines(row, col, board, [{row: 0, col: -1}, {row: 0, col: 1}, 
	                              {row: 1, col: 0}, {row: -1, col: 0}]);
}
